// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3000/api/v2';

export type Code = {
  stdout: String,
  stderr: String,
  exitcode: Number,
};

class TaskService {
  /**
   * Create new task having the given title.
   *
   * Resolves the newly created task id.
   */
  create(data: string) {
    return axios
      .post<{}, {}>('/', { data: data })
      .then((response) => response.data);
  }
}

const taskService = new TaskService();
export default taskService;
